CREATE USER staging PASSWORD '<insert password>';
CREATE USER prod PASSWORD '<insert password>';
CREATE DATABASE "eem-api-staging" WITH OWNER staging;
CREATE DATABASE "eem-api-prod" WITH OWNER prod;
