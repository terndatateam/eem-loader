-- Step 1
-- CREATE a table which is going to hold feature ids and their ultimate study location entity id
-- Tom is a genius!!
DROP TABLE IF EXISTS ULTIMATEPARENT;

WITH RECURSIVE primary_feature AS (
 SELECT
 "relatedFeature" as entity, "relatedFeature" as parent, "relatedFeature" as ultimatestudylocationentityid
 FROM
 featurerelationship
 WHERE
 "relatedFeature" in (select "relatedFeature"
from featurerelationship
where "relatedFeature" not in (select "primaryFeature" from featurerelationship))
 UNION
 SELECT
 child."primaryFeature", child."relatedFeature", parent.ultimatestudylocationentityid
 FROM
 featurerelationship child	
 INNER JOIN primary_feature parent ON parent.entity = child."relatedFeature"
) SELECT
 * INTO ULTIMATEPARENT
FROM
 primary_feature
where entity != ultimatestudylocationentityid;

-- Add an id column - to play nice with SailsJs !
ALTER TABLE ULTIMATEPARENT ADD COLUMN id SERIAL PRIMARY KEY;

------------------------------------------------------------------
------------------------------------------------------------------
-- Create a helper function to manipulating data later
CREATE OR REPLACE FUNCTION eem_last_position(text,char) RETURNS integer AS $$
     select length($1)- length(regexp_replace($1, '.*' || $2,''));
$$ LANGUAGE SQL IMMUTABLE;
-- for use in e.g select substring('http:/revensthorp/R56', eem_last_position('http:/revensthorp/R56', '/') + 1);

-----------------------------------------------------------------
-----------------------------------------------------------------
-- Create the SamplingUnit table
DROP TABLE IF EXISTS SAMPLINGUNIT;

SELECT 
observation.id as "observationId", 
storageset."storageSetUri" as "datasetName", 
observation.feature as "sampledAreaSamplingUnitEntityId", 
ULTIMATEPARENT.ultimatestudylocationentityid as "studyLocationEntityId",
metadata.custodian, 
metadata.rights, 
metadata.citation as "bibliographicReference", 
metadata."dateModified",
metadata.language, 
metadata."persistentSurveyId" as "surveyId", 
metadata."surveyName",
metadata.organisation as "surveyOrganisation", 
metadata.licence
INTO 
SAMPLINGUNIT
FROM 
observation, storageset, ULTIMATEPARENT, surveylink, metadata
WHERE 
observation.feature like '%SAMPL%' 
AND 
observation.feature = ULTIMATEPARENT.entity 
AND 
ULTIMATEPARENT.ultimatestudylocationentityid = surveylink."studyLocation" 
AND 
surveylink.metadata = metadata.id
AND 
observation."storageSet" = storageset.id;

ALTER TABLE SAMPLINGUNIT
 ADD COLUMN id BIGSERIAL PRIMARY KEY,
 ADD COLUMN "stateName" VARCHAR,
 ADD COLUMN "surveyType" VARCHAR, -- Not available yet
 ADD COLUMN "surveyMethodology" VARCHAR, -- Not available yet
 ADD COLUMN "surveyMethodologyDescription" VARCHAR, -- Not available yet
 ADD COLUMN "studyLocationId" VARCHAR,
 ADD COLUMN "originalSiteCode" VARCHAR,
 ADD COLUMN province VARCHAR, -- Not available yet
 ADD COLUMN "geodeticDatum" VARCHAR,
 ADD COLUMN latitude VARCHAR,
 ADD COLUMN longitude VARCHAR,
 ADD COLUMN "locationDescription" VARCHAR, -- Not available yet
 ADD COLUMN aspect VARCHAR, -- Not available yet
 ADD COLUMN slope VARCHAR, -- Not available yet
 ADD COLUMN "landformPattern" VARCHAR, -- Not available yet
 ADD COLUMN "landformElement" VARCHAR, -- Not available yet
 ADD COLUMN elevation VARCHAR, -- Not available yet
 ADD COLUMN "visitId" VARCHAR, -- Not available yet
 ADD COLUMN "visitDate" VARCHAR, -- Not available yet
 ADD COLUMN "visitOrganisation" VARCHAR, -- Not available yet
 ADD COLUMN "visitObservers" VARCHAR, -- Not available yet
 ADD COLUMN "siteDescription" VARCHAR, -- Not available yet
 ADD COLUMN condition VARCHAR, -- Not available yet
 ADD COLUMN "structuralForm" VARCHAR, -- No available
 ADD COLUMN "ownerClassification" VARCHAR, -- Not available yet
 ADD COLUMN "currentClassification" VARCHAR, -- Not available yet
 ADD COLUMN "samplingUnitId" VARCHAR, --Is this the entity id or something else?
 ADD COLUMN "samplingUnitArea" VARCHAR,
 ADD COLUMN "samplingUnitShape" VARCHAR;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- Get studylocation ids (not entity id)
-- e.g aekos.org.au/collection/wa.gov.au/ravensthorpe/R001
-- Load them into a temp table: TEMP_STUDYLOCATION_IDS_DATA

DROP TABLE IF EXISTS TEMP_STUDYLOCATION_IDS_DATA;

SELECT DISTINCT observation.feature as "studyLocationEntityId", 
result.value AS "studyLocationId"
INTO TEMP_STUDYLOCATION_IDS_DATA 
FROM 
observation, lutpropertytype, result
WHERE 
observation."propertyType" = lutpropertytype.id 
AND 
lutpropertytype.name = 'Identifier'
AND 
observation.id = result.observation
AND result.element = 'Value';

-- Update studylocation id in: SAMPLINGUNIT table
UPDATE SAMPLINGUNIT SET "studyLocationId" = (SELECT "studyLocationId" from TEMP_STUDYLOCATION_IDS_DATA where 
SAMPLINGUNIT."studyLocationEntityId" = TEMP_STUDYLOCATION_IDS_DATA."studyLocationEntityId");

-- Update original study location code field in: SAMPLINGUNIT
-- Note: we use the last part of the full studylocation identifier
UPDATE SAMPLINGUNIT SET "originalSiteCode" = substring("studyLocationId", eem_last_position("studyLocationId", '/') + 1);

--Drop the TEMP_STUDYLOCATION_IDS_DATA table - we are done with it
DROP TABLE IF EXISTS TEMP_STUDYLOCATION_IDS_DATA;

------------------------------------------------------------------------------
------------------------------------------------------------------------------
--Get location of studylocation, i.e. lat, lon values
-- Load it in the temp table: TEMP_STUDYLOCATION_LOCATION_DATA

DROP TABLE IF EXISTS TEMP_STUDYLOCATION_LOCATION_DATA;

SELECT DISTINCT observation.feature as "studyLocationEntityId", 
result.element, result.value
INTO TEMP_STUDYLOCATION_LOCATION_DATA 
FROM 
observation, lutpropertytype, result
WHERE  
observation."propertyType" = lutpropertytype.id 
AND 
lutpropertytype.name = 'Location'
AND 
observation.id = result.observation 
AND 
result.element <> 'Type'; 

-- Update Geo Datum field of: SAMPLINGUNIT table
UPDATE SAMPLINGUNIT SET "geodeticDatum" = (SELECT value from TEMP_STUDYLOCATION_LOCATION_DATA where SAMPLINGUNIT."studyLocationEntityId" = TEMP_STUDYLOCATION_LOCATION_DATA."studyLocationEntityId" AND
TEMP_STUDYLOCATION_LOCATION_DATA.element = 'Datum');

-- Update latutude field: in SAMPLINGUNIT table
UPDATE SAMPLINGUNIT SET latitude = (SELECT value from TEMP_STUDYLOCATION_LOCATION_DATA where 
SAMPLINGUNIT."studyLocationEntityId" = TEMP_STUDYLOCATION_LOCATION_DATA."studyLocationEntityId" AND
TEMP_STUDYLOCATION_LOCATION_DATA.element = 'Latitude');

-- Update longitude field: in SAMPLINGUNIT table
UPDATE SAMPLINGUNIT SET longitude = (SELECT value from TEMP_STUDYLOCATION_LOCATION_DATA where 
SAMPLINGUNIT."studyLocationEntityId" = TEMP_STUDYLOCATION_LOCATION_DATA."studyLocationEntityId" AND
TEMP_STUDYLOCATION_LOCATION_DATA.element = 'Longitude');

-- Drop TEMP_STUDYLOCATION_LOCATION_DATA table
DROP TABLE IF EXISTS TEMP_STUDYLOCATION_LOCATION_DATA;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Get sampledArea or samplingUnit size
-- Load them into a temp table TEMP_SAMPLE_AREA_DATA
DROP TABLE IF EXISTS TEMP_SAMPLE_AREA_DATA;

SELECT DISTINCT 
observation.id as "observationId",   
result.element, 
result.value
INTO TEMP_SAMPLE_AREA_DATA 
FROM 
observation, lutpropertytype, result 
WHERE
observation."propertyType" = lutpropertytype.id 
AND 
lutpropertytype.name = 'Area' 
AND
observation.id = result.observation; 

-- Update the Sampling unit id  filed in SAMPLINGUNIT
-- Note: not sure where this data is but for now I am just loading the entity id of the sampledArea or Sampling unit
-- Squid needs to clarify
UPDATE SAMPLINGUNIT SET "samplingUnitId" = SAMPLINGUNIT."sampledAreaSamplingUnitEntityId";

-- Update the sampling unit area field and clean data
UPDATE SAMPLINGUNIT SET "samplingUnitArea" = (SELECT value FROM TEMP_SAMPLE_AREA_DATA WHERE TEMP_SAMPLE_AREA_DATA."observationId" = SAMPLINGUNIT."observationId"
AND TEMP_SAMPLE_AREA_DATA.element = 'Value');

-- Update the sampling unit shape field
UPDATE SAMPLINGUNIT SET "samplingUnitShape" = (SELECT value FROM TEMP_SAMPLE_AREA_DATA WHERE TEMP_SAMPLE_AREA_DATA."observationId" = SAMPLINGUNIT."observationId"
AND TEMP_SAMPLE_AREA_DATA.element = 'Standard');

-- Drop TEMP_SAMPLE_AREA_DATA table
DROP TABLE IF EXISTS TEMP_SAMPLE_AREA_DATA;

-- Create table to hold all the state name in Australia
DROP TABLE IF EXISTS AUSSIESTATE;

CREATE TABLE AUSSIESTATE (
  id serial,
  name varchar,
  UNIQUE(name)
);

INSERT INTO AUSSIESTATE (name) VALUES('New South Whales');
INSERT INTO AUSSIESTATE (name) VALUES('Northern Territory');
INSERT INTO AUSSIESTATE (name) VALUES('Queensland');
INSERT INTO AUSSIESTATE (name) VALUES('South Australia');
INSERT INTO AUSSIESTATE (name) VALUES('Tasmania');
INSERT INTO AUSSIESTATE (name) VALUES('Victoria');
INSERT INTO AUSSIESTATE (name) VALUES('Western Australia');
INSERT INTO AUSSIESTATE (name) VALUES('Nationwide');

--Update stateName field in samplingunit table
--Note: this is a temp fix
-- dataset list: 
   --abares_fgc
   -- adelaide_koonamore
   --dewnr_bdbsa
   --dewnr_roadsideveg
   --dpipwe_platypus
   --oeh_vis
   --qld_corveg
   --tern_ausplots
   --tern_swatt
   --tern_trend
   --uq_supersites_cover
   --usyd_derg
   --wadec_ravensthorpe
UPDATE SAMPLINGUNIT SET "stateName" = 'Nationwide' WHERE "datasetName" = 'abares_fgc';
UPDATE SAMPLINGUNIT SET "stateName" = 'South Australia' WHERE "datasetName" = 'adelaide_koonamore';
UPDATE SAMPLINGUNIT SET "stateName" = 'South Australia' WHERE "datasetName" = 'dewnr_bdbsa';
UPDATE SAMPLINGUNIT SET "stateName" = 'South Australia' WHERE "datasetName" = 'dewnr_roadsideveg';
UPDATE SAMPLINGUNIT SET "stateName" = 'Tasmania' WHERE "datasetName" = 'dpipwe_platypus';
UPDATE SAMPLINGUNIT SET "stateName" = 'New South Whales' WHERE "datasetName" = 'oeh_vis';
UPDATE SAMPLINGUNIT SET "stateName" = 'Queensland' WHERE "datasetName" = 'qld_corveg';
UPDATE SAMPLINGUNIT SET "stateName" = 'Nationwide' WHERE "datasetName" = 'tern_ausplots';
UPDATE SAMPLINGUNIT SET "stateName" = 'Western Australia' WHERE "datasetName" = 'tern_swatt';
UPDATE SAMPLINGUNIT SET "stateName" = 'South Australia' WHERE "datasetName" = 'tern_trend';
UPDATE SAMPLINGUNIT SET "stateName" = 'Queensland' WHERE "datasetName" = 'uq_supersites_cover';
UPDATE SAMPLINGUNIT SET "stateName" = 'New South Whales' WHERE "datasetName" = 'usyd_derg';
UPDATE SAMPLINGUNIT SET "stateName" = 'Western Australia' WHERE "datasetName" = 'wadec_ravensthorpe';

-- Commit
COMMIT;

--END