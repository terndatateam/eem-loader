
-- Step 1
-- CREATE a table which is going to hold feature ids and their ultimate study location entity id
-- Tom is a genius!!
DROP TABLE IF EXISTS ULTIMATEPARENT;

WITH RECURSIVE primary_feature AS (
 SELECT
 "relatedFeature" as entity, "relatedFeature" as parent, "relatedFeature" as ultimatestudylocationentityid
 FROM
 featurerelationship
 WHERE
 "relatedFeature" in (select "relatedFeature"
from featurerelationship
where "relatedFeature" not in (select "primaryFeature" from featurerelationship))
 UNION
 SELECT
 child."primaryFeature", child."relatedFeature", parent.ultimatestudylocationentityid
 FROM
 featurerelationship child
 INNER JOIN primary_feature parent ON parent.entity = child."relatedFeature"
) SELECT
 * INTO ULTIMATEPARENT
FROM
 primary_feature
where entity != ultimatestudylocationentityid;

-- Add an id column - to play nice with SailsJs !
ALTER TABLE ULTIMATEPARENT ADD COLUMN id SERIAL PRIMARY KEY;

-------------------------------------------------------------
-------------------------------------------------------------
-- Create the base Organism table: ORGANISM
DROP TABLE IF EXISTS ORGANISM;

SELECT
     observation.id as "observationId",
     storageset."storageSetUri" as "datasetName",
     observation.feature as "featureId",
     lutfeaturetype.name as "featureOfInterest",
     lutfeaturequalifiertype.name as "featureQualifier",
     lutoriginalfeaturetype.name as "orignalFeatureOfInterest",
     procedure."procedureEntityId" as "protocolLink",
     procedure.name as protocol,
     lutpropertytype.name as property,
     lutpropertyqualifiertype.name as "propertyQualifier"  
INTO ORGANISM   
FROM observation,
     storageset,
     feature,
     lutfeaturetype,
     lutfeaturequalifiertype,
     lutoriginalfeaturetype,
     procedure,
     lutpropertytype,
     lutpropertyqualifiertype 
WHERE 
observation.feature = feature.id AND 
feature."featureType" = lutfeaturetype.id AND 
feature."featureQualifierType" = lutfeaturequalifiertype.id AND 
feature."originalFeatureType" = lutoriginalfeaturetype.id AND 
(lutfeaturetype.name = 'AssemblageOrgGroup' OR 
lutfeaturetype.name = 'SpeciesOrgGroup' OR 
lutfeaturetype.name = 'BioticItem') 
AND observation.procedure = procedure.id AND 
observation."propertyType" = lutpropertytype.id
AND 
observation."storageSet" = storageset.id; 
-- AND observation."propertyQualifierType" = lutpropertyqualifiertype.id;


ALTER TABLE ORGANISM
 ADD COLUMN id BIGSERIAL PRIMARY KEY,
 ADD COLUMN "stateName" VARCHAR,
 ADD COLUMN "surveyId" VARCHAR,
 ADD COLUMN "surveyName" TEXT,
 ADD COLUMN "ultimateFeatureOfInterest" VARCHAR,
 ADD COLUMN value VARCHAR,
 ADD COLUMN "rangeLow" VARCHAR,
 ADD COLUMN "rangeHigh" VARCHAR,
 ADD COLUMN category VARCHAR,
 ADD COLUMN comment VARCHAR,
 ADD COLUMN standard VARCHAR;
 
 -----------------------------------------------------------------
 -----------------------------------------------------------------
-- Process survey id and survey name
DROP TABLE IF EXISTS TEMP_SURVEY_DATA;

SELECT DISTINCT ULTIMATEPARENT.entity, metadata."persistentSurveyId", metadata."surveyName" 
INTO TEMP_SURVEY_DATA 
FROM
ULTIMATEPARENT,
surveylink,
metadata
WHERE
ULTIMATEPARENT.ultimatestudylocationentityid = surveylink."studyLocation" AND
ULTIMATEPARENT.ultimatestudylocationentityid LIKE '%STUDYLOCATION%' AND
surveylink.metadata = metadata.id;

UPDATE ORGANISM SET "surveyId" = (SELECT "persistentSurveyId" FROM TEMP_SURVEY_DATA WHERE ORGANISM."featureId" = TEMP_SURVEY_DATA.entity);
UPDATE ORGANISM SET "surveyName" = (SELECT "surveyName" FROM TEMP_SURVEY_DATA WHERE  ORGANISM."featureId" = TEMP_SURVEY_DATA.entity);  

DROP TABLE IF EXISTS TEMP_SURVEY_DATA;
-----------------------------------------------------------------
-----------------------------------------------------------------
-- Process studylocation
DROP TABLE IF EXISTS TEMP_STUDYLOCATION_LOCATION_DATA;

SELECT DISTINCT ULTIMATEPARENT.entity, ULTIMATEPARENT.ultimatestudylocationentityid, result.value as studylocation_id
INTO TEMP_STUDYLOCATION_LOCATION_DATA 
FROM 
observation, lutpropertytype, result, ULTIMATEPARENT
WHERE 
ULTIMATEPARENT.ultimatestudylocationentityid LIKE '%STUDYLOCATION%' 
AND
ULTIMATEPARENT.ultimatestudylocationentityid = observation.feature
AND 
observation."propertyType" = lutpropertytype.id AND lutpropertytype.name = 'Identifier'
AND 
observation.id = result.observation AND result.element <> 'Type';

UPDATE ORGANISM SET "ultimateFeatureOfInterest" = (SELECT studylocation_id FROM TEMP_STUDYLOCATION_LOCATION_DATA
WHERE ORGANISM."featureId" = TEMP_STUDYLOCATION_LOCATION_DATA.entity);

DROP TABLE IF EXISTS TEMP_STUDYLOCATION_LOCATION_DATA;

-----------------------------------------------------------------
-----------------------------------------------------------------
-- Process height and other information if any
DROP TABLE IF EXISTS TEMP_ORGANISM_DATA;

SELECT DISTINCT ORGANISM."observationId", ORGANISM."featureId", result.element, result.value 
INTO TEMP_ORGANISM_DATA 
FROM 
ORGANISM,
result 
WHERE
ORGANISM."observationId" = result.observation 
AND
result.element NOT IN ('Type')
ORDER BY ORGANISM."observationId" asc;

UPDATE ORGANISM SET value = (SELECT value FROM TEMP_ORGANISM_DATA
WHERE 
ORGANISM."featureId" = TEMP_ORGANISM_DATA."featureId"
AND
ORGANISM."observationId" = TEMP_ORGANISM_DATA."observationId"
AND 
TEMP_ORGANISM_DATA.element = 'Value');

UPDATE ORGANISM SET "rangeLow" = (SELECT value FROM TEMP_ORGANISM_DATA
WHERE ORGANISM."featureId" = TEMP_ORGANISM_DATA."featureId" AND TEMP_ORGANISM_DATA.element = 'RangeLow');

UPDATE ORGANISM SET "rangeHigh" = (SELECT value FROM TEMP_ORGANISM_DATA
WHERE ORGANISM."featureId" = TEMP_ORGANISM_DATA."featureId" AND TEMP_ORGANISM_DATA.element = 'RangeHigh');

UPDATE ORGANISM SET category = (SELECT value FROM TEMP_ORGANISM_DATA
WHERE ORGANISM."featureId" = TEMP_ORGANISM_DATA."featureId" AND TEMP_ORGANISM_DATA.element = 'Category');

UPDATE ORGANISM SET comment = (SELECT value FROM TEMP_ORGANISM_DATA
WHERE ORGANISM."featureId" = TEMP_ORGANISM_DATA."featureId" AND TEMP_ORGANISM_DATA.element = 'Comment');

UPDATE ORGANISM SET standard = (SELECT value FROM TEMP_ORGANISM_DATA
WHERE ORGANISM."featureId" = TEMP_ORGANISM_DATA."featureId" AND TEMP_ORGANISM_DATA.element = 'Standard');

DROP TABLE IF EXISTS TEMP_ORGANISM_DATA;
---------------------------------------------------------
---------------------------------------------------------

--Update stateName field in samplingunit table
--Note: this is a temp fix
-- dataset list: 
   --abares_fgc
   -- adelaide_koonamore
   --dewnr_bdbsa
   --dewnr_roadsideveg
   --dpipwe_platypus
   --oeh_vis
   --qld_corveg
   --tern_ausplots
   --tern_swatt
   --tern_trend
   --uq_supersites_cover
   --usyd_derg
   --wadec_ravensthorpe
UPDATE ORGANISM SET "stateName" = 'Nationwide' WHERE "datasetName" = 'abares_fgc';
UPDATE ORGANISM SET "stateName" = 'South Australia' WHERE "datasetName" = 'adelaide_koonamore';
UPDATE ORGANISM SET "stateName" = 'South Australia' WHERE "datasetName" = 'dewnr_bdbsa';
UPDATE ORGANISM SET "stateName" = 'South Australia' WHERE "datasetName" = 'dewnr_roadsideveg';
UPDATE ORGANISM SET "stateName" = 'Tasmania' WHERE "datasetName" = 'dpipwe_platypus';
UPDATE ORGANISM SET "stateName" = 'New South Whales' WHERE "datasetName" = 'oeh_vis';
UPDATE ORGANISM SET "stateName" = 'Queensland' WHERE "datasetName" = 'qld_corveg';
UPDATE ORGANISM SET "stateName" = 'Nationwide' WHERE "datasetName" = 'tern_ausplots';
UPDATE ORGANISM SET "stateName" = 'Western Australia' WHERE "datasetName" = 'tern_swatt';
UPDATE ORGANISM SET "stateName" = 'South Australia' WHERE "datasetName" = 'tern_trend';
UPDATE ORGANISM SET "stateName" = 'Queensland' WHERE "datasetName" = 'uq_supersites_cover';
UPDATE ORGANISM SET "stateName" = 'New South Whales' WHERE "datasetName" = 'usyd_derg';
UPDATE ORGANISM SET "stateName" = 'Western Australia' WHERE "datasetName" = 'wadec_ravensthorpe';

--Commit
COMMIT;


  