/**
 * Use this to debug a single job. It's far easier than enabling debug
 * for all forked children and either:
 *  1. racing to connect the debugger before your line it hit
 *  2. having every forked child wait for a debugger connection to continue
 *
 * Example:
 *  JOB_NAME='usyd_derg|getHeightObservations2' node inspect ./src/run-single-job.js
 *
 * ...or use your chosen IDE debugger, just make sure to set that environment variable.
 */
const log = require('captains-log')()
const VError = require('verror')
const populateJobs = require('./populate-jobs')
const initFnFactory = require('./cluster-init')
const config = require('./config')

const jobName = process.env.JOB_NAME
const jobs = populateJobs(config)
const initFn = initFnFactory(config)

// we run init to clear the DB and avoid primary key conflicts
const initPromise = initFn()

log.info(`Running single job '${jobName}'`)
initPromise.then(() => {
  return jobs[jobName]()
}).then(() => {
  log.info(`Successfully finished running job='${jobName}'`)
}).catch(err => {
  log.error(`Failed to run job='${jobName}', with error=${VError.fullStack(err)}`)
})
