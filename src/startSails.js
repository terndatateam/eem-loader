const _ = require('@sailshq/lodash')
const eemApiSailsFactory = require('aekos-api-eem/wrapper')

module.exports = async function startSails (dbUrl, migrateStrategy) {
  const sailsConfigOverrides = {
    datastores: {
      default: {
        url: dbUrl
      }
    },
    hooks: {
      orm: require('sails-hook-orm'),
      views: false,
      policies: false,
      security: false,
      pubsub: false,
      session: false,
      i18n: false,
      swagger: false,
      http: false,
      'api-version-accept': false
    },
    log: {
      level: 'warn'
    },
    models: _.merge(require('aekos-api-eem/config/models').models, { migrate: migrateStrategy }),
    port: 33333
  }
  return eemApiSailsFactory(sailsConfigOverrides)
}
