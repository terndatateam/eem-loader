const _ = require('@sailshq/lodash')

/**
 * This module has methods to create records in the tables
 * in the db
 * @param {*} models 
 * @param {*} storageSetId 
 */
module.exports = {
    getCreators: function (models, storageSetId) {
        return {
            createFeature: createFeatureFnFactory(models),
            createFeatureRelationship: createFeatureRelationshipFnFactory(models),
            createMetadata: createMetadataFnFactory(models),
            createObservation: createObservationFnFactory(models),
            createResult: createResultFnFactory(models, storageSetId),
            createProcedure: createProcedureFnFactory(models),
            createSurveyLink: createSurveyLinkFnFactory(models),
            createTime: createTimeFnFactory(models, storageSetId)
        }
    },
    prepModelForPost: function (model){
        return prepModelForPost(model)
    }
}

function createTimeFnFactory(models, storageSetId) {
    return async function createTime(timeType, bindingObj) {
        if (!bindingObj) {
            // FIXME should we handle this in a less silent way?
            return
        }
        const newTime = await models.time.create({
            timeType: timeType,
            epochMs: new Date(bindingObj.value).getTime(),
            storageSet: storageSetId
        }).fetch().catch(err => {
            throw new VError(err, 'Failed to create a "time"')
        })
        return newTime.id
    }
}

function createResultFnFactory(models, storageSetId) {
    return async function createResult(element, bindingObj) {
        if (!bindingObj) {
            // FIXME should we handle this in a less silent way?
            return
        }
        let value
        if (_.isObject(bindingObj)) {
            value = bindingObj.value
        } else { // its just a string
            value = bindingObj
        }
        const newResult = await models.result.create({
            element: element,
            value: value,
            storageSet: storageSetId
        }).fetch().catch(err => {
            throw new VError(err, 'Failed to create a "result"')
        })
        return newResult.id
    }
}

function createFeatureFnFactory(models) {
    return async function createFeature(messyModel) {
        const tidyModel = prepModelForPost(messyModel)
        const newFeature = await models.feature.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, `Failed to create a feature with messyModel='${JSON.stringify(messyModel, null, 2)}' and tidyModel='${JSON.stringify(tidyModel)}'`)
            })
        return newFeature.id
    }
}

function createFeatureRelationshipFnFactory(models) {
    return async function createFeatureRelationship(messyModel) {
        const tidyModel = prepModelForPost(messyModel)
        await models.featurerelationship.create(tidyModel)
            .catch(err => {
                throw new VError(err, 'Failed to create a feature relationship')
            })
    }
}

function createProcedureFnFactory(models) {
    return async function createProcedure(messyModel) {
        const tidyModel = prepModelForPost(messyModel)
        await models.procedure.create(tidyModel)
            .catch(err => {
                throw new VError(err, 'Failed to create a Procedure')
            })
    }
}

function createObservationFnFactory(models) {
    return async function createObservation(messyModel) {
        const tidyModel = prepModelForPost(messyModel)
        const newObservation = await models.observation.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, 'Failed to create an observation')
            })
        return newObservation.id
    }
}

function createMetadataFnFactory(models) {
    return async function createMetadata(messyModel) {
        const tidyModel = prepModelForPost(messyModel)
        const newMetadata = await models.metadata.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, 'Failed to create a metadata')
            })
        return newMetadata.id
    }
}

function createSurveyLinkFnFactory(models) {
    return async function createSurveyLink(messyModel) {
        const tidyModel = prepModelForPost(messyModel)
        await models.surveylink.create(tidyModel)
            .catch(err => {
                throw new VError(err, 'Failed to create a survey link')
            })
    }
}

function prepModelForPost(model) {
    return Object.keys(model).reduce((accum, curr) => {
        const bindingOrArray = model[curr]

        if (_.isUndefined(bindingOrArray) || _.isNull(bindingOrArray)) {
            return accum
        }
        if (Array === bindingOrArray.constructor) {
            const tidyArray = filterUnbound(bindingOrArray)
            accum[curr] = tidyArray
            return accum
        }

        const binding = bindingOrArray

        let value
        if (binding !== null && typeof binding === 'object') {
            value = binding.value
            if (_.isEmpty(value)) { // FIXME we're silently dropping zero length strings here, should we notify?
                return accum
            }
        } else {
            value = binding
        }
        accum[curr] = value
        return accum
    }, {})
}
function filterUnbound(modelArray) {
    return modelArray.filter(e => e)
}
