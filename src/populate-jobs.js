const VError = require('verror')
const log = require('captains-log')()
const _ = require('@sailshq/lodash')
const messageTypes = require('./messageTypes')
const generateStorageSetIds = require('./generateStorageSetIds')
const lookup = require('./look_up_tables')
const modelUtil = require('./model_util')


module.exports = function populateJobs(config) {
  const result = {}
  const tasks = [
    {
      query: 'getSamplingUnitObservations',
      priority: 1,
      fn: async function (binding, storageSetId, models) {

        let featureTypeId = await lookup.getFeatureTypeId(models, binding)
        let featureQualifierTypeId = await lookup.getFeatureQualifierTypeId(models, binding)
        let originalFeatureTypeId = await lookup.getOriginalFeatureTypeId(models, binding)

        const creators = modelUtil.getCreators(models, storageSetId)
        const newFeatureId = await creators.createFeature({
          id: binding.samplingUnit,
          featureType: featureTypeId,
          featureQualifierType: featureQualifierTypeId,
          originalFeatureType: originalFeatureTypeId,
          storageSet: storageSetId
        })

        let relationshipTypeId = await lookup.getRelationshipTypeId(models, binding) // should 'SampleOf'

        await creators.createFeatureRelationship({
          primaryFeature: newFeatureId,
          relationshipType: relationshipTypeId, // 'SampleOf'
          relatedFeature: binding.ultimateFeature // i.e. sl
        })
        await creators.createFeatureRelationship({
          primaryFeature: newFeatureId,
          relationshipType: relationshipTypeId, // 'SampleOf'
          relatedFeature: binding.relatedFeature
        })

        let propertyType = {
          propertyType: {
            value: 'Area'
          }
        }
        // Will use binding once propertType is availabe in sparql query
        let propertyTypeId = await lookup.getPropertyTypeId(models, propertyType)

        let propertyQualifierType = {
          propertyQualifierType: {
            value: 'DSL change or SPARQL'
          }
        }
        let propertyQualifierTypeId = await lookup.getPropertyQualifierTypeId(models, propertyQualifierType)

        // Every feature needs a method/protocol - a Jira has been raized
        const procedureId = await lookup.getProcedureId(models, binding)

        await creators.createObservation({
          feature: newFeatureId,
          ultimateFeature: binding.ultimateFeature,
          propertyType: propertyTypeId,
          propertyQualifierType: propertyQualifierTypeId,
          procedure: procedureId,
          results: [
            await creators.createResult('Type', 'QuantMeasure'),
            await creators.createResult('Value', binding.areaValue),
            await creators.createResult('Standard', binding.areaUnits)
          ],
          // times: [],
          storageSet: storageSetId
          // sensors: []
        })
      }
    },
    {
      query: 'getOrganismGroupBaseData',
      priority: 2,
      fn: async function (binding, storageSetId, models) {

        let featureTypeId = await lookup.getFeatureTypeId(models, binding)
        let featureQualifierId = await lookup.getFeatureQualifierTypeId(models, binding)
        let originalFeatureTypeId = await lookup.getOriginalFeatureTypeId(models, binding)

        const creators = modelUtil.getCreators(models, storageSetId)
        const newFeatureId = await creators.createFeature({
          id: binding.observedItem,
          featureType: featureTypeId,
          featureQualifierType: featureQualifierId,
          originalFeatureType: originalFeatureTypeId,
          storageSet: storageSetId
        })

        // Me thinks we should get relationshipType from SPARQL QRY!!
        let relationshipType = {
          relationshipType: {
            value: 'SampleOf'
          }
        }
        let relationshipTypeId = await lookup.getRelationshipTypeId(models, relationshipType)

        await creators.createFeatureRelationship({
          primaryFeature: newFeatureId,
          relationshipType: relationshipTypeId, //'SampleOf', // or 'UltimateFeatureOfInterest' as in (query)
          relatedFeature: binding.studyLocation
        })
        await creators.createFeatureRelationship({
          primaryFeature: newFeatureId,
          relationshipType: relationshipTypeId, // 'SampleOf', // or 'FeatureOf' as in (query)
          relatedFeature: binding.sampleUnit
        })
      }
    },
    {
      query: 'getHeightObservations2',
      priority: 3,
      fn: async function (binding, storageSetId, models) {
        const creators = modelUtil.getCreators(models, storageSetId)

        let propertyTypeId = await lookup.getPropertyTypeId(models, binding)
        let propertyQualifierTypeId = await lookup.getPropertyQualifierTypeId(models, binding)

        const procedureId = await lookup.getProcedureId(models, binding)

        const newObservation = await creators.createObservation({
          feature: binding.featureOfInterest,
          ultimateFeature: binding.locationID,
          propertyType: propertyTypeId,
          propertyQualifierType: propertyQualifierTypeId,
          procedure: procedureId,
          results: [
            await creators.createResult('Type', 'QuantMeasure'), // FIXME replace with lookup
            await creators.createResult('Value', binding.value),
            await creators.createResult('RangeLow', binding.rangelow),
            await creators.createResult('RangeHigh', binding.rangehigh),
            await creators.createResult('Standard', binding.standard),
            await creators.createResult('Category', binding.category),
            await creators.createResult('Comment', binding.comment)
          ],
          times: [
            await creators.createTime('Phen', binding.dateTime, models, storageSetId)
          ],
          storageSet: storageSetId
          // sensors: []
        })
        // FIXME need to store binding.method somewhere, perhaps in procedure
      }
    },
    {
      query: 'getTaxonObservations',
      priority: 4,
      fn: async function (binding, storageSetId, models) {
        const creators = modelUtil.getCreators(models, storageSetId)
        let propertyTypeId = await lookup.getPropertyTypeId(models, binding)
        let propertyQualifierTypeId = await lookup.getPropertyQualifierTypeId(models, binding)

        const procedureId = await lookup.getProcedureId(models, binding)

        await creators.createObservation({
          feature: binding.observation,
          // ultimateFeature: Feature
          propertyType: propertyTypeId,
          propertyQualifierType: propertyQualifierTypeId,
          procedure: procedureId, // binding.protocol,
          results: [
            await creators.createResult('Type', 'QuantMeasure'), // FIXME replace with lookup
            await creators.createResult('Value', binding.speciesName)
          ],
          times: [
            await creators.createTime('Phen', binding.dateTime)
          ],
          storageSet: storageSetId
          // sensors: []
        })
      }
    },
    {
      query: 'mapSurveyToStudyLocation',
      priority: 5,
      fn: async function (binding, storageSetId, models) {
        const surveyRecordId = await lookup.getSurveyEntityId(models, binding)
        const creators = modelUtil.getCreators(models, storageSetId)
        await creators.createSurveyLink({
          studyLocation: binding.studyLocation,
          studyLocationSubgraphId: binding.studyLocationSubGraph,
          metadata: surveyRecordId,
        })
      }
    },
    {
      query: 'getStudyLocations',
      priority: 6,
      fn: async function (binding, storageSetId, models) {

        let featureTypeId = await lookup.getFeatureTypeId(models, binding)
        let featureQualifierTypeId = await lookup.getFeatureQualifierTypeId(models, binding)
        let originalFeatureTypeId = await lookup.getOriginalFeatureTypeId(models, binding)

        const creators = modelUtil.getCreators(models, storageSetId)
        const studyLocationFeature = await creators.createFeature({
          id: binding.studyLocation,
          featureType: featureTypeId,
          featureQualifierType: featureQualifierTypeId,
          originalFeatureType: originalFeatureTypeId,
          storageSet: storageSetId
        })

        const procedureId = await lookup.getProcedureId(models, binding)

        // TODO - should come from SPAQRL
        let identifierPropertyType = {
          propertyType: {
            value: 'Identifier'
          }
        }
        let propertyTypeId = await lookup.getPropertyTypeId(models, identifierPropertyType)

        // TODO - should come from SPAQRL
        let primaryPropertyQualifierType = {
          propertyQualifierType: {
            value: 'Primary'
          }
        }
        let propertyQualifierTypeId = await lookup.getPropertyQualifierTypeId(models, primaryPropertyQualifierType)

        await creators.createObservation({
          feature: studyLocationFeature,
          // ultimateFeature: Feature
          propertyType: propertyTypeId,
          propertyQualifierType: propertyQualifierTypeId,
          procedure: procedureId,
          results: [
            await creators.createResult('Type', 'SimpleValue'),
            await creators.createResult('Value', binding.persistentLocationIdentifier)
          ],
          // times: [],
          storageSet: storageSetId
          // sensors: []
        })

        // TODO - should come from SPAQRL
        let locationPropertyType = {
          propertyType: {
            value: 'Location'
          }
        }
        propertyTypeId = await lookup.getPropertyTypeId(models, locationPropertyType)

        // TODO - should come from SPAQRL
        let coordinatePropertyQualifierType = {
          propertyQualifierType: {
            value: 'Coordinate'
          }
        }
        propertyQualifierTypeId = await lookup.getPropertyQualifierTypeId(models, coordinatePropertyQualifierType)

        await creators.createObservation({
          feature: studyLocationFeature,
          // ultimateFeature: Feature
          propertyType: propertyTypeId, // 'Location',
          propertyQualifierType: propertyQualifierTypeId, // 'Coordinate',
          procedure: procedureId,
          results: [
            await creators.createResult('Type', 'Coordinate'),
            await creators.createResult('Datum', 'GDA94'),
            await creators.createResult('Longitude', binding.longitude),
            await creators.createResult('Latitude', binding.latitude),
          ],
          // times: [],
          storageSet: storageSetId
          // sensors: []
        })
      }
    },
  ]

  for (const currDataset of generateStorageSetIds(config)) {
    const storageSetId = currDataset.id
    const datasetName = currDataset.name
    for (const currTask of tasks) {
      validateTask(currTask, tasks)
      const jobName = `${datasetName}|${currTask.query}`
      const workerFn = buildWorkerFn(config, datasetName, currTask, storageSetId, jobName)
      result[jobName] = {priority: currTask.priority, jobFn: workerFn}
    }
  }
  return result
}

function validateTask(task, tasks) {
  if (!task.query) {
    throw new VError(`Programmer problem: task with no query defined, task='${JSON.stringify(task)}',` +
      ` tasks='${JSON.stringify(tasks, null, 2)}'`)
  }
}

function buildWorkerFn(config, dataset, task, storageSetId, jobName) {
  const queryForSparql = require('./queryForSparql')
  const startSails = require('./startSails')
  return async function (pageNum) {
    const sails = await startSails(config.pgUrl, 'safe')
    const offset = pageNum * config.pageSize
    const bindings = await queryForSparql(config, task.query, dataset, offset)
    notifyMasterIfMorePages(config, pageNum, bindings.length, jobName)
    await processPage(config, bindings, storageSetId, sails.models, task.fn, jobName, pageNum)
    const lowerPromise = new Promise((resolve, reject) => {
      sails.lower(err => {
        if (err) {
          return reject(new VError(err, 'Failed to lower sails'))
        }
        return resolve()
      })
    })
    await lowerPromise
    const recordsProcessed = bindings.length
    return recordsProcessed
  }
}

async function processPage(config, bindings, storageSetId, models, recordProcessor, jobName, pageNum) {
  const bindingLength = bindings.length
  const prefix = `${timestamp()} [${jobName}]`
  log.debug(`${prefix} Processing ${bindingLength} bindings, page ${pageNum}`)
  return new Promise((resolve, reject) => {
    if (bindingLength === 0) {
      return resolve(0)
    }
    asyncForEach(bindings, async function (e, index) {
      await recordProcessor(e, storageSetId, models)
      if (index && index % config.progressLogCheckpoint === 0) {
        log.info(`${prefix} Processed ${index}/${bindingLength} bindings of page ${pageNum}`)
      }
    }).then(() => {
      return resolve(bindingLength)
    }).catch(err => {
      return reject(new VError(err))
    })
  })
}

async function asyncForEach(array, callback) {
  const parallelLimit = require('async/parallelLimit')
  const functions = []
  for (let index = 0; index < array.length; index++) {
    const fn = async function () {
      await callback(array[index], index)
    }
    functions.push(fn)
  }
  const limit = 100 // balance this between CPU usage and disk thrashing
  return new Promise((resolve, reject) => {
    // parallelLimit seems to take tasks in chunks, rather than executing in order
    //   so our progress logging is a bit off. It'll say when the function with some
    //   loop index has executed, but that doesn't mean all the ones before it have finished.
    parallelLimit(functions, limit, err => {
      if (err) {
        return reject(new VError(err))
      }
      return resolve()
    })
  })
}

function notifyMasterIfMorePages(config, pageNum, bindingLength, jobName) {
  const isMorePages = bindingLength === config.pageSize
  if (!isMorePages) {
    return
  }
  const nextPageNum = ++pageNum
  process.send({
    type: messageTypes.morePages,
    pageNum: nextPageNum,
    jobName: jobName
  })
}

function timestamp() {
  const now = new Date()
  return `${now.toLocaleString()}`
}

