const VError = require('verror')
const _ = require('@sailshq/lodash')
const modelUtil = require('./model_util')

module.exports = {
    getFeatureTypeId: async function getFeatureTypeId(models, binding) {
        const lookups = getLookups(models)
        const lookupCreators = getLookupCreators(models)
        let featureTypeId

        if (_.has(binding, 'featureType') && binding.featureType.value) {
            let results = await lookups.lookupFeatureType(binding.featureType.value)
            if (results.length > 0) {
                featureTypeId = results[0].id
            } else {
                let newFeatureTypeId = await lookupCreators.createFeatureType({
                    name: binding.featureType
                })
                if (newFeatureTypeId) {
                    featureTypeId = newFeatureTypeId
                }
            }
        }
        return featureTypeId
    },

    getFeatureQualifierTypeId: async function getFeatureQualifierTypeId(models, binding) {
        const lookups = getLookups(models)
        const lookupCreators = getLookupCreators(models)
        let featureQualifierTypeId

        if (_.has(binding, 'featureQualifier') && binding.featureQualifier.value) {
            let results = await lookups.lookupFeatureQualifierType(binding.featureQualifier.value)
            if (results.length > 0) {
                featureQualifierTypeId = results[0].id
            } else {
                let newFeatureQualifierTypeId = await lookupCreators.createFeatureQualifierType({
                    name: binding.featureQualifier
                })
                if (newFeatureQualifierTypeId) {
                    featureQualifierTypeId = newFeatureQualifierTypeId
                }
            }
        }
        return featureQualifierTypeId
    },

    getOriginalFeatureTypeId: async function getOriginalFeatureTypeId(models, binding) {
        const lookups = getLookups(models)
        const lookupCreators = getLookupCreators(models)
        let originalFeatureTypeId

        if (_.has(binding, 'originalFeatureType') && binding.originalFeatureType.value) {
            let results = await lookups.lookupOriginalFeatureType(binding.originalFeatureType.value)
            if (results.length > 0) {
                originalFeatureTypeId = results[0].id
            } else {
                let newOriginalFeatureTypeId = await lookupCreators.createOriginalFeatureType({
                    name: binding.originalFeatureType
                })
                if (newOriginalFeatureTypeId) {
                    originalFeatureTypeId = newOriginalFeatureTypeId
                }
            }
        }
        return originalFeatureTypeId
    },

    getPropertyTypeId: async function getPropertyTypeId(models, binding) {
        const lookups = getLookups(models)
        const lookupCreators = getLookupCreators(models)
        let propertyTypeId

        if (_.has(binding, 'propertyType') && binding.propertyType.value) {
            let results = await lookups.lookupPropertyType(binding.propertyType.value)
            if (results.length > 0) {
                propertyTypeId = results[0].id
            } else {
                let newPropertyTypeId = await lookupCreators.createPropertyType({
                    name: binding.propertyType
                })
                if (newPropertyTypeId) {
                    propertyTypeId = newPropertyTypeId
                }
            }
        }
        return propertyTypeId
    },

    getPropertyQualifierTypeId: async function getPropertyQualifierTypeId(models, binding) {
        const lookups = getLookups(models)
        const lookupCreators = getLookupCreators(models)
        let propertyQualifierTypeId

        if (_.has(binding, 'propertyQualifierType') && binding.propertyQualifierType.value) {
            let results = await lookups.lookupPropertyQualifierType(binding.propertyQualifierType.value)
            if (results.length > 0) {
                propertyQualifierTypeId = results[0].id
            } else {
                let newPropertyQualifierTypeId = await lookupCreators.createPropertyQualifierType({
                    name: binding.propertyQualifierType
                })
                if (newPropertyQualifierTypeId) {
                    propertyQualifierTypeId = newPropertyQualifierTypeId
                }
            }
        }
        return propertyQualifierTypeId
    },

    getRelationshipTypeId: async function getRelationshipTypeId(models, binding) {
        const lookups = getLookups(models)
        const lookupCreators = getLookupCreators(models)
        let relationshipTypeId

        if (_.has(binding, 'relationshipType') && binding.relationshipType.value) {
            let results = await lookups.lookupRelationshipType(binding.relationshipType.value)
            if (results.length > 0) {
                relationshipTypeId = results[0].id
            } else {
                let newRelationshipTypeId = await lookupCreators.createRelationshipType({
                    name: binding.relationshipType
                })
                if (newRelationshipTypeId) {
                    relationshipTypeId = newRelationshipTypeId
                }
            }
        }
        return relationshipTypeId
    },
    getSurveyEntityId: async function getSurveyEntityId(models, binding){
        const lookups = getLookups(models)
        let surveyRecordId
        if (_.has(binding, 'surveySubGraph') && binding.surveySubGraph.value){
           surveyRecordId = await lookups.lookupSurveyRecordId(binding.surveySubGraph.value)
        }
        return surveyRecordId
    },
    getProcedureId: async function getProcedureId(models, binding){
        const lookups = getLookups(models)
        let procedureRecordId
        if (_.has(binding, 'protocol') && binding.protocol.value){
            procedureRecordId = await lookups.lookupProcedureRecordId(binding.protocol.value)
        }
        return procedureRecordId
    }

}

function getLookupCreators(models) {
    return {
        createFeatureType: createFeatureTypeFnFactory(models),
        createFeatureQualifierType: createFeatureQualifierTypeFnFactory(models),
        createOriginalFeatureType: createOriginalFeatureTypeFnFactory(models),
        createRelationshipType: createRelationshipTypeFnFactory(models),
        createPropertyType: createPropertyTypeFnFactory(models),
        createPropertyQualifierType: createPropertyQualifierTypeFnFactory(models),
        createOriginalPropertyType: createOriginalPropertyTypeFnFactory(models),
    }
}

function getLookups(models) {
    return {
        lookupFeatureType: lookupFeatureTypeFactory(models),
        lookupFeatureQualifierType: lookupFeatureQualifierTypeFactory(models),
        lookupOriginalFeatureType: lookupOriginalFeatureTypeFactory(models),
        lookupRelationshipType: lookupRelationshipTypeFactory(models),
        lookupPropertyType: lookupPropertyTypeFactory(models),
        lookupPropertyQualifierType: lookupPropertyQualifierTypeFactory(models),
        lookupOriginalPropertyType: lookupOriginalPropertyTypeFactory(models),
        lookupSurveyRecordId: lookupSurveyRecordIdFactory(models),
        lookupProcedureRecordId: lookupProcedureIdFactory(models),
    }
}

function createFeatureTypeFnFactory(models) {
    return async function createFeatureType(messyModel) {
        const tidyModel = modelUtil.prepModelForPost(messyModel)
        const newFeatureType = await models.lutfeaturetype.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, 'Failed to insert a feature type lookup value')
            })
        return newFeatureType.id
    }
}

function createFeatureQualifierTypeFnFactory(models) {
    return async function createFeatureQualifierType(messyModel) {
        const tidyModel = modelUtil.prepModelForPost(messyModel)
        const newFeatureQualifierType = await models.lutfeaturequalifiertype.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, 'Failed to insert a feature qualifier type lookup value')
            })
        return newFeatureQualifierType.id
    }
}

function createOriginalFeatureTypeFnFactory(models) {
    return async function createOriginalFeatureType(messyModel) { 
        const tidyModel = modelUtil.prepModelForPost(messyModel) 
        const newOriginalFeatureType = await models.lutoriginalfeaturetype.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, 'Failed to insert an original feature type lookup value')
            })  
        return newOriginalFeatureType.id
    }
}

function createRelationshipTypeFnFactory(models) {
    return async function createRelationshipType(messyModel) {
        const tidyModel = modelUtil.prepModelForPost(messyModel)
        const newRelationshipType = await models.lutrelationshiptype.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, 'Failed to insert a relationship type lookup value')
            })
        return newRelationshipType.id
    }
}

function createPropertyTypeFnFactory(models) {
    return async function createPropertyType(messyModel) {
        const tidyModel = modelUtil.prepModelForPost(messyModel)
        const newPropertyType = await models.lutpropertytype.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, 'Failed to insert a property type lookup value')
            })
        return newPropertyType.id
    }
}

function createPropertyQualifierTypeFnFactory(models) {
    return async function createPropertyQualifierType(messyModel) {
        const tidyModel = modelUtil.prepModelForPost(messyModel)
        const newPropertyQualifierType = await models.lutpropertyqualifiertype.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, 'Failed to insert a property qualifier type lookup value')
            })
        return newPropertyQualifierType.id
    }
}

function createOriginalPropertyTypeFnFactory(models) {
    return async function createOriginalPropertyType(messyModel) {
        const tidyModel = modelUtil.prepModelForPost(messyModel)
        const newOriginalPropertyType = await models.lutoriginalpropertytype.create(tidyModel)
            .fetch()
            .catch(err => {
                throw new VError(err, 'Failed to insert an original property type lookup value')
            })
        return newOriginalPropertyType.id
    }
}

// LOOKUP values getters
function lookupFeatureTypeFactory(models) {
    return async function lookup(name) {
        const result = await models.lutfeaturetype.find({ name: name })
        return result
    }
}
function lookupFeatureQualifierTypeFactory(models) {
    return async function lookup(name) {
        const result = await models.lutfeaturequalifiertype.find({ name: name })
        return result
    }
}
function lookupOriginalFeatureTypeFactory(models) {
    return async function lookup(name) {
        const result = await models.lutoriginalfeaturetype.find({ name: name })
        return result
    }
}
function lookupRelationshipTypeFactory(models) {
    return async function lookup(name) {
        const result = await models.lutrelationshiptype.find({ name: name })
        return result
    }
}
function lookupPropertyTypeFactory(models) {
    return async function lookup(name) {
        const result = await models.lutpropertytype.find({ name: name })
        return result
    }
}
function lookupPropertyQualifierTypeFactory(models) {
    return async function lookup(name) {
        const result = await models.lutpropertyqualifiertype.find({ name: name })
        return result
    }
}
function lookupOriginalPropertyTypeFactory(models) {
    return async function lookup(name) {
        const result = await models.lutoriginalpropertytype.find({ name: name })
        return result
    }
}
function lookupSurveyRecordIdFactory(models) {
    return async function lookup(surveyEntityId) {
        const results = await models.metadata.find({ surveyEntityId: surveyEntityId })
        if (results.length > 0) {
            return results[0].id
        }else {
            return null // No record found
        }
    }
}

function lookupProcedureIdFactory(models) {
    return async function lookup(procedureEntityId) {
        const results = await models.procedure.find({ procedureEntityId: procedureEntityId })
        if (results.length > 0) {
            return results[0].id
        }else {
            return null // No record found
        }
    }
}
