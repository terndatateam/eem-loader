const log = require('captains-log')()
const cluster = require('cluster')
const os = require('os')
const VError = require('verror')
const prettyMs = require('pretty-ms')
const messageTypes = require('./messageTypes')

const isFailFast = !process.env.EEM_NO_FAIL_FAST // set to anything to be truthy

/**
 * Executes a queue of jobs over multiple processors until the queue
 * is exhausted.
 *
 * @param {{jobName: {fn: Function, priority: number}}} jobs key is the job name, value is the *async* worker function
 * @param {Function} initFn async function to do one-time setup before any other tasks start
 */
module.exports = function runCluster(jobs, initFn, postJobsFn) {
  if (cluster.isMaster) {
    if (!initFn) {
      initMaster(jobs, postJobsFn)
      return
    }
    log.info('Running init function first')
    const startMs = new Date().getTime()
    initFn().then(() => {
      const elapsedMs = new Date().getTime() - startMs
      log.info(`init function ran in ${prettyMs(elapsedMs)}`)
      return initMaster(jobs, postJobsFn)
    }).catch(err => {
      const chainedErr = new VError(err, 'Failed to execute the init function')
      log.error(`Error while executing init function`, VError.fullStack(chainedErr))
      process.exit(1)
    })
    return
  }

  runWorker(jobs)
}

function initMaster(jobs, postJobsFn) {
  const start = new Date().getTime()
  // TODO might need a task-graph to ensure completion of A before starting B
  const allJobs = Object.keys(jobs).reduce((accum, curr) => {
    const firstPage = 0
    accum.push({
      jobName: curr,
      priority: jobs[curr].priority,
      pageNum: firstPage
    })
    return accum
  }, [])
  allJobs.sort((a, b) => a.priority - b.priority)
  const initialJobCount = allJobs.length
  let totalJobCount = initialJobCount
  const results = []
  let finishedWorkerCount = 0

  log.info(totalJobCount, 'jobs queued')

  const handlers = {
    [messageTypes.end]: message => {
      results.push(message)
      if (!message.success && isFailFast) {
        log.error(`job='${message.job}' failed, so failing-fast for everything`)
        process.exit(1)
      }
    },
    [messageTypes.morePages]: message => {
      const jobName = message.jobName
      const pageNum = message.pageNum
      const newJob = {
        jobName: jobName,
        pageNum: pageNum
      }
      const isWorkerFree = allJobs.length === 0
      allJobs.push(newJob)
      totalJobCount++
      log.debug(`added new job='${jobName}' for page='${pageNum}', remaining jobs='${allJobs.length}'`)
      if (isWorkerFree) {
        log.debug(`workers are available, scheduling job='${jobName}' with page='${pageNum}'`)
        sendJob()
      }
    }
  }
  cluster.on('message', function (worker, message) {
    const type = message.type
    const handler = handlers[type]
    if (!handler) {
      log.error(`Programmer error: no handler for message type='${type}'`)
      return
    }
    handler(message)
  })
  // triggered when worker dies
  cluster.on('exit', function (worker, code, signal) {
    finishedWorkerCount++
    const isMoreJobs = allJobs.length > 0
    if (isMoreJobs) {
      log.info('Job finished, sending another')
      sendJob()
    }
    // once parent dies workers will also die, wait for all to finish
    const isAllTasksDone = finishedWorkerCount === totalJobCount
    if (isAllTasksDone) {
      function printStatsAndExit() {
        const elapsedMs = new Date().getTime() - start
        log.info(`Elapsed wall clock time ${prettyMs(elapsedMs)}`)
        const successfulJobs = results.filter(e => e.success)
        const failedJobs = results.filter(e => !e.success)
        const isFailedJobs = failedJobs.length
        log.info(`==Results==`)
        const addedJobCount = totalJobCount - initialJobCount
        log.info(`Initial jobs=${initialJobCount}, total jobs=${totalJobCount}, ${addedJobCount} added`)
        log.info(`Successful jobs:`)
        sortAndPrint(successfulJobs)
        if (isFailedJobs) {
          log.info(`Failed jobs:`)
          sortAndPrint(failedJobs)
        }
        process.exit()
      }
      // Create Organism and SamplingUnit tables
      const postJobsPromise = postJobsFn();
      postJobsPromise.then(result => {
        log.info(result[0])
        log.info(result[1])
        printStatsAndExit()
      })
        .catch(err => {
          log.error(err)
          printStatsAndExit()
        })
    }
  })

  // spawn workers
  const isMoreThan1Cpu = os.cpus().length > 1
  const maxWorkerCount = isMoreThan1Cpu ? os.cpus().length - 1 : 1
  const workerCount = Math.min(totalJobCount, maxWorkerCount)
  log.info(`Using up to ${workerCount} workers`)
  for (let i = 0; i < workerCount; i++) {
    sendJob().catch(err => {
      log.error(`Failed to start a worker, with error=${err}`)
    })
  }

  async function sendJob() {
    const job = allJobs.pop()
    const isDebug = !!process.env.EEM_DEBUG // true to enable debug on *all* forked processes
    const env = await getForkEnv(isDebug)
    const worker = cluster.fork(env)
    const jobName = job.jobName
    const pageNum = job.pageNum
    worker.send({
      jobName,
      pageNum
    })
    log.debug(`Forked a worker for job='${jobName}', page='${pageNum}' with PID='${worker.process.pid}'`)
  }

  /**
   * We fork child processes. If you want to debug them, you probably can't from
   * your IDE because only the master has the debug flag set. To debug a child,
   * set isDebug to true, then connect your debugger.
   *
   * Connect chrome devtools by opening URL:
   *   chrome-devtools://devtools/bundled/inspector.html?v8only=true&ws=${wsUrl}
   * where wsUrl=127.0.0.1:9229/b15d8ec4-d251-4d23-8e89-d297c1c0dcaa or whatever the process prints to stdout.
   *
   * For more discussion, see:
   * https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27
   *
   * @param {boolean} isDebug true to enable debugging of child processes
   */
  async function getForkEnv(isDebug) {
    if (!isDebug) {
      return {}
    }
    const debugPort = await require('get-port')({ port: 9229 })
    const pauseForDebuggerConnect = !!process.env.EEM_DEBUG_BRK // truthy to wait for debugger connection
    const breakFragment = pauseForDebuggerConnect ? '-brk' : ''
    const result = {
      'NODE_OPTIONS': `--inspect${breakFragment}=${debugPort}`
    }
    return result
  }
}

function runWorker(jobs) {
  process.on('message', function (assignedJob) {
    const jobName = assignedJob.jobName
    const pageNum = assignedJob.pageNum
    const jobFn = jobs[jobName].jobFn
    log.debug(`child is running job='${jobName}' for page='${pageNum}'`)
    const startMs = new Date().getTime()
    const jobPromise = jobFn(pageNum)
    jobPromise.then(recordsProcessed => {
      process.send({
        type: messageTypes.end,
        success: true,
        job: jobName,
        pageNum: pageNum,
        recordsProcessed: recordsProcessed,
        elapsed: new Date().getTime() - startMs
      })
      process.exit()
    }).catch(err => {
      log.error(`Error while processing job='${jobName}'`, VError.fullStack(err))
      process.send({
        type: messageTypes.end,
        success: false,
        job: jobName,
        pageNum: pageNum,
        elapsed: new Date().getTime() - startMs
      })
      process.exit()
    })
  })
}

function sortAndPrint(results) {
  results.sort(compareChildResults)
  results.forEach(e => {
    const rp = isNaN(e.recordsProcessed) ? '' : `, processed ${e.recordsProcessed} recs`
    log.info(`  ${e.job}, page=${e.pageNum}${rp} (${prettyMs(e.elapsed)})`)
  })
}

function compareChildResults(a, b) {
  const jobNameCompareResult = simpleCompare(a.job, b.job)
  if (jobNameCompareResult !== 0) {
    return jobNameCompareResult
  }
  return simpleCompare(a.pageNum, b.pageNum)
}

function simpleCompare(a, b) {
  if (a < b) {
    return -1
  }
  if (a > b) {
    return 1
  }
  return 0
}
