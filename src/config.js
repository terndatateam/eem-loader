// Make pageSize large enough that you don't waste too much time on overhead
// submitting queries, but short enough that if you have workers freeing up
// near the end of the run, they can share some load. Perhaps aim for something
// that means ~5-10 minutes runtime per job. That way, you'll only have to deal
// with ~30 minutes (I'm guessing) of less than full resource utilisation.
// Remember that paging through SPARQL results means the server runs the query
// again and discards everything up to the offset you want, it's not free!
const pageSize = 10000 // 2500
const defaultPgUrl = 'postgresql://docker:docker@localhost:5432/docker3'
const defaultSparqlUrl = 'http://129.127.180.97:8083/fuseki/aekos2/query'

function getPageSize() {
  return process.env.EEM_LOADER_PAGE_SIZE || pageSize
}
function getPgUrl() {
  const pgUrl = process.env.EEM_LOADER_PG || defaultPgUrl
  return pgUrl
}

function getSparqlServerQueryUrl() {
  const sparqlUrl = process.env.SPARQL_SERVER_QUERY_URL || defaultSparqlUrl
  return sparqlUrl
}

module.exports = {
  pgUrl: getPgUrl(),
  sparqlServerQueryUrl: getSparqlServerQueryUrl(),
  pageSize: getPageSize(),
  progressLogCheckpoint: 500,
  datasetsToProcess: [
    // 'abares_fgc',
    // 'adelaide_koonamore',
     'dewnr_bdbsa',
    // 'dewnr_roadsideveg',
    // 'dpipwe_platypus',
     'oeh_vis',
     'qld_corveg',
    // 'tern_ausplots',
    // 'tern_swatt',
    // 'tern_trend',
    // 'uq_supersites_cover',
    //  'usyd_derg',
    // 'wadec_ravensthorpe'
  ],
  // TODO make this config /\
  datasetPlaceholder: '%TARGET_DATASET%'
}
