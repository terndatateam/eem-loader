/**
 * General utility functions
 */
module.exports = {
    /**
     * Null safe way to get ms time for number table column.
     * We need to check becuase new Date(null) returns the epoch.
     */
    dateStringToMs: function (bindingObj) {
        if (!bindingObj) {
            return null
        }
        let date = new Date(bindingObj).getTime()
        return date
    }
}
