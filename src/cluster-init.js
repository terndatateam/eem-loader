const log = require('captains-log')()
const VError = require('verror')
const parallelLimit = require('async/parallelLimit')
const startSails = require('./startSails')
const generateStorageSetIds = require('./generateStorageSetIds')
const queryForSparql = require('./queryForSparql')
const utils = require('./general_utils')
const modelUtil = require('./model_util')

module.exports = function factory(config) {
  return async function init() {
    log.info(`Using pageSize = ${config.pageSize}`)
    assertDepsReady()
    const sails = await startSailsAndDropDatabase()
    await createStorageSets(sails)
    await createLookupTables(sails)
    await createAndLoadProtocolTable(sails)
    await createAndLoadSurveyMetadataTable(sails)
    await lowerSails(sails)
  }

  function assertDepsReady() {
    const fs = require('fs')
    const eemApiDir = './node_modules/aekos-api-eem'
    const files = fs.readdirSync(`${eemApiDir}/node_modules/`)
    const isEemApiHasAllDeps = files.length > 50
    if (!isEemApiHasAllDeps) {
      const msg = `Missing dependencies!
        The aekos-api-eem project needs all of its dependencies installed
        because sails will look there when lifting. Yeah, it's dirty but
        we have to do it. To fix this, run:

          pushd ${eemApiDir} && rm -fr node_modules/ && yarn && popd
        `
      log.error(new Error(msg))
      process.exit(1)
    }
  }

  async function startSailsAndDropDatabase() {
    try {
      const migrate = process.env.EEM_LOADER_SAILS_DB_MIGRATE || 'drop'
      log.info(`Using DB Migrate = ${migrate}`)
      const sails = await startSails(config.pgUrl, migrate)
      return sails
    } catch (error) {
      throw new VError(error, 'Failed to lift sails')
    }
  }

  function createStorageSets(sails) {
    const storageSets = generateStorageSetIds(config)
    const tasks = []
    for (const curr of storageSets) {
      const task = async function () {
        await sails.models.storageset.create({
          id: curr.id,
          storageSetUri: curr.name
        })
      }
      tasks.push(task)
    }
    return new Promise((resolve, reject) => {
      parallelLimit(tasks, 100, err => {
        if (err) {
          return reject(new VError(err, 'Failed to create storage sets'))
        }
        return resolve()
      })
    })
  }

  async function createLookupTables(sails) {
    const tasks = []
    function addTasks(modelName, names) {
      for (const curr of names) {
        const task = async function () {
          await sails.models[modelName.toLowerCase()].create({
            // id: curr, // should we hash this, or convert to a number?
            name: curr
          }).catch(err => {
            throw new VError(err, `Failed why trying to create a ${modelName}`)
          })
        }
        tasks.push(task)
      }
    }
    addTasks('LutFeatureType', [
      'BioticItem',
      'AssemblageOrgGroup',
      'SpeciesOrgGroup',
      'SamplingUnit',
      'SampledArea',
      'StudyLocation'
    ])
    addTasks('LutFeatureQualifierType', [
      'Individual',
      'Stand',
      'Quadrat',
      'Rectangular'
      // TODO more data in getSamplingUnitObservations
    ])
    addTasks('LutOriginalFeatureType', [
      'dummy', // FIXME need to get this populated in the queries
      'Sampled Area',
      'Sampling Plot',
      'Patch',
      'Stratum sampling unit',
      'Ground-cover Sampling Transect'
    ])
    addTasks('LutRelationshipType', [
      'SampleOf'
      // FIXME do we need UltimateFeatureOfInterest and FeatureOf?
    ])
    addTasks('LutPropertyType', [
      'Name',
      'Height',
      'Identifier',
      'Location',
      'Area',
    ])
    // FIXME if page size is too small, we'll miss things, need a query-for-all mode
    const propQualTypeBindings = await queryForSparql(config, 'getPropertyQualifierTypes', '', 0)
    const propQualTypesFromData = propQualTypeBindings.reduce((accum, curr) => {
      accum.push(curr.propertyQualifierType.value)
      return accum
    }, [])
    const extraPropQualTypes = [
      'AVG',
      'Primary',
      'Coordinate',
      // 'Field Species Concept',
      // 'Determined Species Concept',
      'DSL change or SPARQL'
    ]

    const allPropQualTypes = extraPropQualTypes.concat(propQualTypesFromData)

    addTasks('LutPropertyQualifierType', allPropQualTypes)

    return new Promise((resolve, reject) => {
      parallelLimit(tasks, 100, err => {
        if (err) {
          return reject(new VError(err))
        }
        return resolve()
      })
    })
  }

  function lowerSails(sails) {
    return new Promise(async (resolve, reject) => {
      sails.lower(err => {
        if (err) {
          return reject(new VError(err, 'Failed to lower sails'))
        }
        return resolve()
      })
    })
  }

  /**
   * Mosheh
   * @param {*} sails 
   */
  function createAndLoadProtocolTable(sails) {
    const offset = 0 // pageNum * config.pageSize
    let sparqlQuery = 'getProtocols' // aka. Procedure
    const tasks = []
    for (const currDataset of generateStorageSetIds(config)) {
      const task = async function () {

        log.info(`Loading Procedures/Protocols for [${currDataset.name}]`)

        const bindings = await queryForSparql(config, sparqlQuery, currDataset.name, offset)
        for (let i = 0; i < bindings.length; i++) {
          const creators = modelUtil.getCreators(sails.models, currDataset.id)
          await creators.createProcedure({
            procedureEntityId: bindings[i].methodId,
            name: bindings[i].name,
            summary: bindings[i].abstract,
            details: bindings[i].detail
          })
        }
        log.info(`Finished loading Procedures/Protocols for [${currDataset.name}]`)
      }
      tasks.push(task)
    }

    return new Promise((resolve, reject) => {
      parallelLimit(tasks, 100, err => {
        if (err) {
          return reject(new VError(err, 'Failed to create and load protocol/procedure table'))
        }
        return resolve()
      })
    })
  }

  function createAndLoadSurveyMetadataTable(sails) {
    const offset = 0 // pageNum * config.pageSize
    let sparqlQuery = 'getSurveyDetails'
    const tasks = []
    for (const currDataset of generateStorageSetIds(config)) {
      const task = async function () {

        log.info(`Loading Metadata for [${currDataset.name}]`)

        const bindings = await queryForSparql(config, sparqlQuery, currDataset.name, offset)
        for (let i = 0; i < bindings.length; i++) {
          const creators = modelUtil.getCreators(sails.models, currDataset.id)
          await creators.createMetadata({
            surveyEntityId: bindings[i].svsg,
            persistentSurveyId: bindings[i].persistentSurveyIdentifier,
            surveyName: bindings[i].surveyTitle,
            organisation: bindings[i].organisationNameFixed,
            custodian: bindings[i].custodianName,
            rights: bindings[i].rightsStatementFixed,
            licence: bindings[i].licenceName,
            citation: bindings[i].citationTextFixed,
            dateModified: utils.dateStringToMs(bindings[i].modificationDate.value),
            dateAccessioned: utils.dateStringToMs(bindings[i].accessionDate.value),
            language: bindings[i].language,
            surveyType: 'SurvType', // FIXME
            surveyMethod: 'SurvMethod', // FIXME
            methodLink: 'MethodLink' // FIXME
          })
        }
        log.info(`Finished loading Metadata for [${currDataset.name}]`)
      }
      tasks.push(task)
    }
    return new Promise((resolve, reject) => {
      parallelLimit(tasks, 100, err => {
        if (err) {
          return reject(new VError(err, 'Failed to create and load metadata table'))
        }
        return resolve()
      })
    })
  }
}
