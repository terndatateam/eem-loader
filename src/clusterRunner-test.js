const clusterRunner = require('./clusterRunner')

const jobs = (function populateJobs () {
  const result = {}
  const tasks = ['query1', 'query2', 'query3', 'query4', 'query5']
  const datasets = ['wadec', 'ausplots', 'corveg']

  for (const dataset of datasets) {
    for (const task of tasks) {
      const jobName = `${dataset}|${task}`
      const workerFn = async function () {
        console.log(`processing task=${task} for dataset=${dataset}`)
        await new Promise(resolve => setTimeout(resolve, 2000))
      }
      result[jobName] = workerFn
    }
  }
  return result
})()

function init () {
  return new Promise((resolve, reject) => {
    console.log('3 second inital setup')
    setTimeout(function () {
      console.log('setup done')
      resolve()
    }, 3000)
  })
}

clusterRunner(jobs, init)
