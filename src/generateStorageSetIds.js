module.exports = function generateStorageSetIds (config) {
  let idCounter = 1
  const result = config.datasetsToProcess.reduce((accum, curr) => {
    accum.push({
      id: idCounter++,
      name: curr
    })
    return accum
  }, [])
  return result
}
