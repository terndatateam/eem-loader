const async = require('async');
const VError = require('verror');
const log = require('captains-log')();
const pg = require('pg');
const fs = require('fs');
var parse = require('pg-connection-string').parse;
const config = require('../config');
const path = require("path");

const organismScript = 'eem_generate_organism_table.sql';
const samplingUnitScript = 'eem_generate_samplingunit_table.sql';

const dbConf = parse(config.pgUrl);
const pool = new pg.Pool(dbConf);

async function runScript() {

    const organism = 'Organism';
    const samplingUnit = 'SamplingUnit';
    const mainTasks = [];

    const organismSql = fs.readFileSync(path.resolve(__dirname, `../db_scripts/${organismScript}`)).toString();
    mainTasks.push({ taskName: organism, sql: organismSql });

    const samplingUnitSql = fs.readFileSync(path.resolve(__dirname, `../db_scripts/${samplingUnitScript}`)).toString();
    mainTasks.push({ taskName: samplingUnit, sql: samplingUnitSql });

    const tasks = [];
    for (const item of mainTasks) {
        const task = () => {
            return new Promise((resolve, reject) => {
                pool.connect((err, client, done) => {
                    log.info(`Generating ${item.taskName} table now. Please wait...`);
                    if (err) {
                        return reject(new VError(err, 'Error trying to connect to postgres.'))
                    }
                    client.query(item.sql, (err, result) => {
                        done();
                        if (err) {
                            return reject(new VError(err, `Error trying to generate ${item.taskName} table.`))
                        }
                        return resolve(`Finished generating ${item.taskName} table.`)
                    });
                });
            })
        }
        tasks.push(task);
    }

    const [result1, result2] = await Promise.all([tasks[0](), tasks[1]()]);
    return [result1, result2];
}

module.exports = async () => {
    return runScript();
}

// runScript();



