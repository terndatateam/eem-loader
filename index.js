const clusterRunner = require('./src/clusterRunner')
const populateJobs = require('./src/populate-jobs')
const initFnFactory = require('./src/cluster-init')
const config = require('./src/config')
const generateOrganismAndSamplingUnitTables = require('./src/postgres/load_organism_samplingunit');


const jobs = populateJobs(config)
const initFn = initFnFactory(config)
clusterRunner(jobs, initFn, generateOrganismAndSamplingUnitTables)
