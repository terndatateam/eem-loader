> Batch job that queries the AEKOS RDF store and populates the EEM API database

## Quickstart

  1. clone the repo
  1. install all the deps

        yarn

  1. do a hack so that Sails will lift properly

        pushd ./node_modules/aekos-api-eem
        yarn
        popd

  1. start a Docker Postgres, if you don't already have postgres running, start one with:

        docker run \
          -e POSTGRES_USER=docker \
          -e POSTGRES_PASSWORD=docker \
          -e POSTGRES_DB=docker \
          -p 5432:5432 \
          --detach \
          --name=eem-pg \
          postgres:10

  1. run the job (Note: currently all config is hardcoded inside `src/config.js`)

        node index.js


## Debugging
Debugging isn't as straght forward as you might expect because we fork child processes. You can debug the master as normal but if you want to debug the children, you have two options.

  1. Option #1: run only the job you want to debug. This is the easiest option. Do this by using the `./src/run-single-job.js` file. See the file for more instructions.
  1. Option #2: run all child with debug enabled. This is more work because you either need to race to connect the debugger or enable `break` mode where each process will wait for a debugger to connect before continuing. You can see that neither is really that nice to work with. See `./src/clusterRunner.js:sendJob()` for more details.

## Architecture
### Language
We have some choices when it comes to how to implement this tool. We need to read RDF data, that is currently handled by Apache JENA/Fuseki (Java) and write it to the database. Ideally we would use the SailsJS Waterline ORM to write the data to the DB so we don't repeat ourselves with the schema.

  1. **NodeJS:** we can submit SPARQL queries via HTTP to Fuseki from a NodeJS program, then parse the results and write to the DB using Waterline.
  1. **Java:** we can directly read the RDF data using the JENA API and then make an HTTP call to the running SailsJS API to write the data.

In both cases, we need to make HTTP calls. The important thing is to minimise the overhead we suffer from that. Option 1 (NodeJS) is preferable because we can query for large pages of SPARQL results (batching) and then process them all. Option 2 (Java) would mean we would need to build something to allow batch POSTing to the Sails API as this isn't supported out-of-the-box.

### Concurrency
A limitation of the NodeJS option is that it's single threaded. The event loop will avoid blocking but it's still only going to use 1 processor. To utilise the CPU fully, we'll need to use [clustering](https://nodejs.org/api/cluster.html#cluster_cluster_fork_env) to spawn child processes. As part of this, we'll need to chunk our work up by paging through SPARQL queries and farming it out to workers.

NodeJS, on 64 bit, also has a 1.7GB memory limit. Be aware that we can increase that using `--max-old-space-size=<megabytes>` if required.

### Selection
The reasons we're using SailsJS for the API are:

  1. fast startup (crucial for running inside AWS Lambda)
  1. out-of-the-box RESTful API features including automatic routes for models [(Blueprints)](https://sailsjs.com/documentation/concepts/blueprints)
  1. ORM abstraction so we have a choice of databases

If we explore options outside the box, we could look at alternatives for the API that aren't written in NodeJS but still satisfy the requirements. [Vertx](https://vertx.io/) is written in Java and have very quick startup. It's more like Express than Sails however, in that we would still need to do a fair bit of work ourselves. If we find an alternative to Sails in the Java world, we could avoid any HTTP calls during loading and perhaps even do all the work inside one VM.

### Rationale
Our goal for the loader is to completely max out one of the available resources (CPU, memory, disk). We run multiple processes; 1 for the master and then each task is forked into a child process, up until each CPU core has 1 process. Each of the child processes utilises the ability of NodeJS to avoid blocking via promises, async/await or callbacks. The means we're using all the cores of a CPU and if the network/disk can keep up, we'll max out the CPU.

We combat the problem of uneven sized jobs by dynamically queuing more jobs to handle more pages of data. The initial job queue is to process the first page for everything. Inside each of those jobs, once we receive the query result set but before we start processing, we can tell if there's another page of results. If there is, we push another job for that page onto the job queue. This way, even if one dataset+query combo is bigger than every other job combined, we can still spread the load over all CPUs and power through it. We don't waste time up front trying to compute how many pages there are but we pay for this feature in terms of complexity of this software.

### Database
We're using Postgres as our database because it's familiar to us. Waterline, the ORM for SailsJS, doesn't create referential integrity, only primary keys. This is good because it means we can pump data in faster with less overhead and in any order. It's bad because we need to health check the data after a load to make sure everything lines up.

## TODO

  1. total up job runtime and record count over all pages in final summary
  1. add a post-run function to the clusterRunner so we can add referential integrity
